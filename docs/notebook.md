# Jupyter Notebook





Rappelons que Jupyter Notebook peut être obtenu en installant [anaconda](https://www.anaconda.com/distribution/).
Vous pouvez aussi plus directement passer par [jupyter.org](https://jupyter.org/).

!!! note
    Les notes d'installation concerne vos machines perso à la maison, pas les machines du lycée.

Pour une installation de python complète sur vos machines, installez [anaconda](https://www.anaconda.com/distribution/)!

JupyterLab a un fonctionnement analogue, vous pouvez l'utiliser en lieu et place de Jupyter Notebook.


 

!!! important
    Tout au long de l'année, pour tester les exemples de cours, pour tester vos réponses aux exercices, nous
    vous conseillons d'utiliser systématiquement jupyter notebook.  
    L'usage d'un tel interpréteur vous permettra, entre autre gain de temps, de n'avoir pas à ajouter
    systématiquement des instructions print pour l'affichage des résultats.



!!! note
    Il est possible d'utiliser jupyter notebook en ligne sans installation sur sa machine:
    
    + [jupyter.org](https://jupyter.org/try)

##  Ouverture

Dans la page d'ouverture de Anaconda Navigator, cliquez sur le bouton "launch" (lancer) de Jupyter Notebook.

![](img_executer/notebooklaunch.png)

Dans la fenêtre qui s'ouvre, sélectionnez (à droite) "Nouveau" puis python3.

![](img_executer/nouveau.png)

## Entrer le texte d'une fonction


Dans la cellule qui se présente, entrer le code d'une fonction.
Par exemple:

 
![](img_executer/notebook1.png)

Cliquez sur "Exécuter" dans le menu ou appuyez sur <span style="font-size:1.5rem;">⇑</span> + entrée



## Testez  une fonction

Dans la cellule suivante, vous pouvez alors tester votre fonction en entrant par exemple `double(7)`
puis <span style="font-size:1.5rem;">⇑</span> + entrée


![](img_executer/notebook2.png)


??? note
    [lien sur le fichier ipynb](img_executer/notebook.ipynb)  
    [lien sur une version statique html de ce fichier](img_executer/notebook.html)  
    
    
    
## Une vidéo

Le notebook permet également d'ajouter du texte entre les cellules python, ce qui permet de présenter 
des scripts de façon très détaillée.

Vous pouvez par exemple visualiser 
[cette vidéo](http://maths.enseigne.ac-lyon.fr/spip/spip.php?article649&lang=fr).
    
 
