# Le notebook avec Capytale


Nous allons voir plusieurs outils pour exécuter du code Python.

Nous commençons par l'outil Capytale. 

##  Intéret
Un avantage de cet outil: si vous l'utilisez en classe:

+ vous retrouverez vos scripts chez vous sans problème,
+ et sans installation d'outils sur votre ordinateur.

Par contre, il vous faudra bien nommer vos feuilles au fur et à mesure sous peine de ne plus vous y retrouver. Commencer -- par exemple -- toujours par nommer une feuille par le nom du chapitre concerné, suivi du titre du paragraphe et du numéro d'exercice.

##  Prise en main
Pour prendre en main le notebook vous pouvez le faire:

+ soit directement dans ELéa
+ soit via l'ENT par le menu ressources numériques/capytale.

Il faut traiter avec rigueur l'activité Eléa : Prendre en main capytale



