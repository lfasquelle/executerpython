# Exécuter un programme Python.
 
 
 

Vous utiliserez durant l'année plusieurs façons d'exécuter vos scripts Python:

+ Un interpréteur lancé depuis un terminal.  
+ Un éditeur de texte.
+ Un IDE, par exemple Spyder.
+ Un "super" interpréteur: Jupyter Notebook.



 
Sur vos machines personnelles à la maison,  la distribution 
[anaconda](https://www.anaconda.com/) peut être un bon choix: vous disposerez en l'installant
de python, de nombreux packages python et
de plusieurs outils comme jupyter notebook, jupyterLab, Spyder...
