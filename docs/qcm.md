# QCM



## QCM 1

Le langage Python a été créé en:

- [ ] 2001
- [ ] 2011
- [ ] 1991
- [ ] 1981


??? solution "Réponse"


    - [ ] 2001
    - [ ] 2011
    - [X] 1991
    - [ ] 1981
    
    
    

## QCM 2

Le langage Python a été créé par:

- [ ] Guido van Rossum
- [ ] La troupe Monty Python
- [ ] Linus Torvalds
- [ ] Tim Berners-Lee


??? solution "Réponse"


    - [X] Guido van Rossum
    - [ ] La troupe Monty Python
    - [ ] Linus Torvalds
    - [ ] Tim Berners-Lee
    
    
    
## QCM 3

Pour écrire puis exécuter un programme Python, on peut utiliser

- [ ] un logiciel de traitement de texte
- [ ] un éditeur de texte


??? solution "Réponse"


    - [ ] un logiciel de traitement de texte
    - [X] un éditeur de texte


    Veillez à ne pas confondre ces deux types de logiciel. Dans un texte écrit via un traitement de texte,
    beaucoup d'informations supplémentaires (comme la fonte, la mise en gras, etc...) sont enregistrées et ces 
    informations ne peuvent être comprises par l'interpréteur python.
    
    Pour vous rendre compte de la quantité d'informations supplémentaires enregistrées avec un traitement de 
    texte, créez un fichier texte avec LibreOffice Writer. Tapez quelques mots et enregistrez le fichier.
    Faites ensuite un clic droit sur le fichier et sélectionnez "extraire ici". Un dossier est créé: il contient
    les divers fichiers d'informations complémentaires de votre fichier .odt (il s'agit essentiellement
    d'informations au format xml). Vous pouvez ouvrir ces fichiers
    avec un éditeur de texte.
    
    
    
## QCM 4


Le code python 

```
>>> 4^3
```

donne le résultat:

- [ ] 12
- [ ] 7
- [ ] 64


??? solution "Réponse"

    - [ ] 12
    - [X] 7
    - [ ] 64
    
    L'important pour le moment n'est pas que vous sachiez expliquer que le résultat est 7 mais
    que vous sachiez expliquer pourquoi le résultat **n'est pas** 64:
     rappelons que 4<sup>3</sup> est obtenu par `4**3` en python.
    
    
    !!! note
        Nous expliquons pourquoi 7. Vous pourrez revenir sur cette explication et la comprendre
        lorsque nous aurons expliqué l'écriture binaire et l'opérateur xor.
        
        [L'opérateur `^`](https://docs.python.org/fr/3/reference/expressions.html?highlight=bitwise#binary-bitwise-operations) 
        est un ou exclusif bit à bit.
        On écrit 4 et 3 en binaire: 4 = 100<sub>deux</sub> et 3 =  011<sub>deux</sub>.
        On effectue ensuite un xor (ou exclusif) bit à bit:
        
        <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg .tg-cly1{text-align:left;vertical-align:middle}
        </style>
        <table class="tg">
          <tr>
            <th class="tg-cly1"></th>
            <th class="tg-cly1">1</th>
            <th class="tg-cly1">0</th>
            <th class="tg-cly1">0</th>
          </tr>
          <tr>
            <td class="tg-cly1">xor</td>
            <td class="tg-cly1">0</td>
            <td class="tg-cly1">1</td>
            <td class="tg-cly1">1</td>
          </tr>
          <tr style="border-top: 5px solid black;">
            <td class="tg-cly1"></td>
            <td class="tg-cly1">1</td>
            <td class="tg-cly1">1</td>
            <td class="tg-cly1">1</td>
          </tr>
        </table>
        
        Et 111<sub>deux</sub> = 7.
        
        
        
        
## QCM 5


Le code Python

```
>>> 16//3
```

donne:

- [ ] 5
- [ ] 5.333333333333333
- [ ] 5.0


??? solution "Réponse"

    - [X] 5
    - [ ] 5.333333333333333
    - [ ] 5.0
    
    16 =  5 &times; 3 + 1. D'où le résultat (quotient entier de 16 par 5).
    
    Remarque: par contre, le résultat de `16/3` est le flottant 5.333333333333333.
    
    
    
## QCM 6 

On utilise l'arborescence suivante:

![](img_executer/arbo.png)


On a ouvert un terminal. Le répertoire de travail est D.

Pour que le répertoire de travail devienne C, on utilise:

- [ ] `cd ../../C`
- [ ] `cd C`
- [ ] `cd ../C`
- [ ] `cd ../../../A/C`
 
 
??? solution "Réponse"



    - [X] `cd ../../C`
    - [ ] `cd C`
    - [ ] `cd ../C`
    - [X] `cd ../../../A/C`



  
    
## QCM 7 

On utilise l'arborescence suivante:

![](img_executer/arbo.png)


On a ouvert un terminal. Le répertoire de travail est B.

On veut lister le contenu du répertoire D. On utilise: 

- [ ] `ls D`
- [ ] `ls D/`
- [ ] `ls`
- [ ] `cd D` puis `ls`
 
 
??? solution "Réponse"


    - [X] `ls D`
    - [X] `ls D/`
    - [ ] `ls`
    - [X] `cd D` puis `ls`

     
