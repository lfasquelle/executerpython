# Avec un éditeur dédié



 


## Le logiciel Geany  

Ce logiciel dispose des fonctions élémentaires d'un environnement de développement intégré.

Dans un éditeur tel que Geany, vous pourrez exécuter vos scripts directement en cliquant sur un bouton
de l'interface de l'éditeur (à condition d'avoir paramétré correctement les chemins,
 mais cela se fait une fois pour toutes).
 
 
## Indenter


Geany automatise l'indentation lorsqu'on travaille sur un fichier 
d'extension .py. Par exemple, taper `if 2>3:` suivi de entrée, la ligne suivante commence 
avec une indentation de 4 espaces.


Attention, toutefois, si vous créez une indentation manuellement à l'aide de la touche de tabulation plutôt
que par 4 espaces, vous risquez de créer un bug (mélange d'indentations différentes: une tabulation ou 4 espaces).
Geany permet de règler ce problème en vous permettant  de faire en sorte qu'un appui sur la touche tabulation 
ait le même effet que 4 appuis successifs sur la barre d'espace.  
Pour cela, il suffit de sélectionner dans le menu Document/Type d'indentation/Espaces.


## Exécuter

Si les chemins vers Python ont été paramétrés correctement, pour exécuter un programme python écrit dans un 
fichier .py, il suffit, si l'on a ouvert ce fichier avec geany, de cliquer sur l'icône ![](img_executer/roueExec.png) ou de sélectionner 
le menu Construire/Execute.


Les chemins vers python sont paramétrés dans Construire/Définir les commandes de construction.
Vous veillerez notamment dans la première et la dernière ligne de ce menu
que soit inscrit python3, et non python. On peut en effet avoir plusieurs versions de python sur une même machine 
et une inscription "python" marque un chemin vers python2. Nous ne travaillerons qu'en Python 3.

![](img_executer/construire.png)



## Un tutoriel
 

[Un tutoriel Geany.](https://deusyss.developpez.com/tutoriels/Python/Geany/)






 
