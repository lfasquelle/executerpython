# Un court historique du langage Python



## Auteur

Le langage Python a été créé par le néerlandais [Guido van Rossum](https://fr.wikipedia.org/wiki/Guido_van_Rossum).

Guido van Rossum a reçu en 2002 le prix du logiciel libre.


Guido van Rossum était fan de la troupe des [Monty python](https://fr.wikipedia.org/wiki/Monty_Python), c'est 
là l'origine du nom de ce langage.

## Les versions 


La première version de ce langage date de 1991.

La version 2.0 de Python date de 2000.

La version 3.0 date de 2008.

La version 3.7 date de janvier 2018.

Pour connaître le numéro de la dernière version en cours, consultez le site [python.org](https://www.python.org/).




