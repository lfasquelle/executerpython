"""
Ce fichier contient des scripts de fonctions de référence.
"""


def carre(x):
    """
    x -- nombre (int ou float)

    renvoie le carré de x
    """
    return x*x


def cube(x):
    """
    x -- nombre (int ou float)

    renvoie le cube de x
    """
    return x * carre(x)


def inverse(x):
    """
    x -- nombre (int ou float) non nul

    renvoie l'inverse de x
    """
    return 1/x
