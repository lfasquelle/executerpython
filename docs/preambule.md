# Que signifie **programmer** un ordinateur? 







Programmer consiste à créer des suites d'instructions destinées à l'ordinateur. 
Un ordinateur sans programme ne sait rien faire. 

Il existe différents langages qui permettent de programmer. 
Le seul directement utilisable par le processeur est le **langage machine** (suite de 1 et de 0). 
Aujourd'hui on ne programme pratiquement plus directement en langage machine (c'est "lourd"
et difficile à maintenir).


Au lieu d'un langage machine, on utilise maintenant en général des **langages de haut niveau**, c'est à dire
des langages qui utilisent des instructions explicites et directement compréhensibles
par le programmeur plutôt que des suites de 0 et de 1. 

Ces instructions seront ensuite traduites en langage machine par un programme spécialisé. 
Ce système de traduction s'appellera **interpréteur** ou bien **compilateur**, 
suivant la méthode utilisée pour effectuer la traduction.

On peut séparer les langages de programmation en deux "familles":

+ Les **langages de bas niveau**  qui sont complexes à utiliser, car très éloignés du langage naturel. 
Ce sont des langages « proches de la machine ». 
En contrepartie, ils permettent de faire des programmes très rapides à l'exécution. 
L'assembleur est un langage de bas niveau. 
Certains "morceaux" de programmes sont écrits en assembleur encore aujourd'hui.

+ Les **langages de haut niveau** sont eux plus "faciles" à utiliser, 
car plus proches du langage naturel (exemple : si a = 3 alors b ← c).   
Exemples de langages de haut niveau : C, C++ , Java, Python, PHP, javascript...  
On peut ensuite, à l'intérieur de cette famille de langages de haut niveau, à nouveau "hiérarchiser": le langage C
(création au début des années 1970)
par exemple est de plus bas niveau que le langage Python (création début des années 1990).  

En NSI, le [programme officiel](http://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)
impose d'utiliser essentiellement le langage [Python](https://www.python.org/about/).


